import { NgModule } from '@angular/core'

import { AuthConfigModule } from '@colmena/module-admin-auth'
import { ContentConfigModule } from '@colmena/module-admin-content'
import { CoreConfigModule } from '@colmena/module-admin-core'
import { DashboardConfigModule } from '@colmena/module-admin-dashboard'
import { DataBrowserConfigModule } from '@colmena/module-admin-data-browser'
import { DevConfigModule } from '@colmena/module-admin-dev'
import { StorageConfigModule } from '@colmena/module-admin-storage'
import { SystemConfigModule } from '@colmena/module-admin-system'


let admin = [
  CoreConfigModule,
  DevConfigModule,
  StorageConfigModule,
  SystemConfigModule,
  DataBrowserConfigModule,
]

let roles = JSON.parse(window.localStorage.getItem('roles'))
if(roles && roles.unassigned.indexOf('system-admin')!=-1) {
  admin = []
}

console.log(admin);


@NgModule({
  imports: [
    AuthConfigModule,
    ContentConfigModule,
    DashboardConfigModule,
    ...admin
  ],
})
export class AppConfigModule {}
