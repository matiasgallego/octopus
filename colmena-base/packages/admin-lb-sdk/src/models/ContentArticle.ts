/* tslint:disable */
import {
  StorageFile,
  SystemDomain,
  SystemUser
} from '../index';

declare var Object: any;
export interface ContentArticleInterface {
  "entry"?: string;
  "entry_url"?: string;
  "author"?: string;
  "author_url"?: string;
  "date"?: Date;
  "location"?: string;
  "location_url"?: string;
  "id"?: number;
  "systemDomainId"?: string;
  "systemUserId"?: string;
  "storageFileId"?: string;
  "created"?: Date;
  "modified"?: Date;
  storageFile?: StorageFile;
  systemDomain?: SystemDomain;
  systemUser?: SystemUser;
}

export class ContentArticle implements ContentArticleInterface {
  "entry": string;
  "entry_url": string;
  "author": string;
  "author_url": string;
  "date": Date;
  "location": string;
  "location_url": string;
  "id": number;
  "systemDomainId": string;
  "systemUserId": string;
  "storageFileId": string;
  "created": Date;
  "modified": Date;
  storageFile: StorageFile;
  systemDomain: SystemDomain;
  systemUser: SystemUser;
  constructor(data?: ContentArticleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `ContentArticle`.
   */
  public static getModelName() {
    return "ContentArticle";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of ContentArticle for dynamic purposes.
  **/
  public static factory(data: ContentArticleInterface): ContentArticle{
    return new ContentArticle(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'ContentArticle',
      plural: 'ContentArticles',
      properties: {
        "entry": {
          name: 'entry',
          type: 'string'
        },
        "entry_url": {
          name: 'entry_url',
          type: 'string'
        },
        "author": {
          name: 'author',
          type: 'string'
        },
        "author_url": {
          name: 'author_url',
          type: 'string'
        },
        "date": {
          name: 'date',
          type: 'Date'
        },
        "location": {
          name: 'location',
          type: 'string'
        },
        "location_url": {
          name: 'location_url',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "systemDomainId": {
          name: 'systemDomainId',
          type: 'string'
        },
        "systemUserId": {
          name: 'systemUserId',
          type: 'string'
        },
        "storageFileId": {
          name: 'storageFileId',
          type: 'string'
        },
        "created": {
          name: 'created',
          type: 'Date'
        },
        "modified": {
          name: 'modified',
          type: 'Date'
        },
      },
      relations: {
        storageFile: {
          name: 'storageFile',
          type: 'StorageFile',
          model: 'StorageFile'
        },
        systemDomain: {
          name: 'systemDomain',
          type: 'SystemDomain',
          model: 'SystemDomain'
        },
        systemUser: {
          name: 'systemUser',
          type: 'SystemUser',
          model: 'SystemUser'
        },
      }
    }
  }
}
