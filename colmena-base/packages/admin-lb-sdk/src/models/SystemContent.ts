/* tslint:disable */

declare var Object: any;
export interface SystemContentInterface {
  "id": string;
  "name": string;
  "systemDomainId"?: string;
}

export class SystemContent implements SystemContentInterface {
  "id": string;
  "name": string;
  "systemDomainId": string;
  constructor(data?: SystemContentInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `SystemContent`.
   */
  public static getModelName() {
    return "SystemContent";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of SystemContent for dynamic purposes.
  **/
  public static factory(data: SystemContentInterface): SystemContent{
    return new SystemContent(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'SystemContent',
      plural: 'SystemContents',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "systemDomainId": {
          name: 'systemDomainId',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
