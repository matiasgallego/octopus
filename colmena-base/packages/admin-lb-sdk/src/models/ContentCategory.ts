/* tslint:disable */
import {
  ContentEntity,
  ContentTag,
  StorageFile,
  SystemDomain,
  SystemUser
} from '../index';

declare var Object: any;
export interface ContentCategoryInterface {
  "name": string;
  "title"?: string;
  "description"?: string;
  "has_entities"?: boolean;
  "created"?: Date;
  "priority"?: number;
  "html"?: string;
  "image"?: string;
  "media"?: string;
  "file"?: string;
  "id"?: number;
  "systemDomainId"?: string;
  "systemUserId"?: string;
  "storageFileId"?: string;
  "modified"?: Date;
  ContentEntities?: ContentEntity[];
  ContentTags?: ContentTag[];
  storageFile?: StorageFile;
  systemDomain?: SystemDomain;
  systemUser?: SystemUser;
}

export class ContentCategory implements ContentCategoryInterface {
  "name": string;
  "title": string;
  "description": string;
  "has_entities": boolean;
  "created": Date;
  "priority": number;
  "html": string;
  "image": string;
  "media": string;
  "file": string;
  "id": number;
  "systemDomainId": string;
  "systemUserId": string;
  "storageFileId": string;
  "modified": Date;
  ContentEntities: ContentEntity[];
  ContentTags: ContentTag[];
  storageFile: StorageFile;
  systemDomain: SystemDomain;
  systemUser: SystemUser;
  constructor(data?: ContentCategoryInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `ContentCategory`.
   */
  public static getModelName() {
    return "ContentCategory";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of ContentCategory for dynamic purposes.
  **/
  public static factory(data: ContentCategoryInterface): ContentCategory{
    return new ContentCategory(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'ContentCategory',
      plural: 'ContentCategories',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "title": {
          name: 'title',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "has_entities": {
          name: 'has_entities',
          type: 'boolean',
          default: false
        },
        "created": {
          name: 'created',
          type: 'Date'
        },
        "priority": {
          name: 'priority',
          type: 'number',
          default: 0
        },
        "html": {
          name: 'html',
          type: 'string'
        },
        "image": {
          name: 'image',
          type: 'string'
        },
        "media": {
          name: 'media',
          type: 'string'
        },
        "file": {
          name: 'file',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "systemDomainId": {
          name: 'systemDomainId',
          type: 'string'
        },
        "systemUserId": {
          name: 'systemUserId',
          type: 'string'
        },
        "storageFileId": {
          name: 'storageFileId',
          type: 'string'
        },
        "modified": {
          name: 'modified',
          type: 'Date'
        },
      },
      relations: {
        ContentEntities: {
          name: 'ContentEntities',
          type: 'ContentEntity[]',
          model: 'ContentEntity'
        },
        ContentTags: {
          name: 'ContentTags',
          type: 'ContentTag[]',
          model: 'ContentTag'
        },
        storageFile: {
          name: 'storageFile',
          type: 'StorageFile',
          model: 'StorageFile'
        },
        systemDomain: {
          name: 'systemDomain',
          type: 'SystemDomain',
          model: 'SystemDomain'
        },
        systemUser: {
          name: 'systemUser',
          type: 'SystemUser',
          model: 'SystemUser'
        },
      }
    }
  }
}
