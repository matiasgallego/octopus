/* tslint:disable */
import {
  ContentTag,
  StorageFile,
  SystemDomain,
  SystemUser
} from '../index';

declare var Object: any;
export interface ContentCardInterface {
  "title"?: string;
  "description"?: string;
  "created"?: Date;
  "order"?: number;
  "html"?: string;
  "image"?: string;
  "media"?: string;
  "file"?: string;
  "id"?: number;
  "systemDomainId"?: string;
  "systemUserId"?: string;
  "storageFileId"?: string;
  "modified"?: Date;
  ContentTags?: ContentTag[];
  storageFile?: StorageFile;
  systemDomain?: SystemDomain;
  systemUser?: SystemUser;
}

export class ContentCard implements ContentCardInterface {
  "title": string;
  "description": string;
  "created": Date;
  "order": number;
  "html": string;
  "image": string;
  "media": string;
  "file": string;
  "id": number;
  "systemDomainId": string;
  "systemUserId": string;
  "storageFileId": string;
  "modified": Date;
  ContentTags: ContentTag[];
  storageFile: StorageFile;
  systemDomain: SystemDomain;
  systemUser: SystemUser;
  constructor(data?: ContentCardInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `ContentCard`.
   */
  public static getModelName() {
    return "ContentCard";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of ContentCard for dynamic purposes.
  **/
  public static factory(data: ContentCardInterface): ContentCard{
    return new ContentCard(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'ContentCard',
      plural: 'ContentCards',
      properties: {
        "title": {
          name: 'title',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "created": {
          name: 'created',
          type: 'Date'
        },
        "order": {
          name: 'order',
          type: 'number',
          default: 0
        },
        "html": {
          name: 'html',
          type: 'string'
        },
        "image": {
          name: 'image',
          type: 'string'
        },
        "media": {
          name: 'media',
          type: 'string'
        },
        "file": {
          name: 'file',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "systemDomainId": {
          name: 'systemDomainId',
          type: 'string'
        },
        "systemUserId": {
          name: 'systemUserId',
          type: 'string'
        },
        "storageFileId": {
          name: 'storageFileId',
          type: 'string'
        },
        "modified": {
          name: 'modified',
          type: 'Date'
        },
      },
      relations: {
        ContentTags: {
          name: 'ContentTags',
          type: 'ContentTag[]',
          model: 'ContentTag'
        },
        storageFile: {
          name: 'storageFile',
          type: 'StorageFile',
          model: 'StorageFile'
        },
        systemDomain: {
          name: 'systemDomain',
          type: 'SystemDomain',
          model: 'SystemDomain'
        },
        systemUser: {
          name: 'systemUser',
          type: 'SystemUser',
          model: 'SystemUser'
        },
      }
    }
  }
}
