/* tslint:disable */
import {
  ContentEvent,
  ContentPage,
  ContentProduct,
  ContentPost,
  ContentCategory,
  ContentEntity,
  ContentCard,
  ContentTag,
  ContentArticle,
  StorageFile,
  SystemContent
} from '../index';

declare var Object: any;
export interface SystemDomainInterface {
  "id": string;
  "name": string;
  "email": string;
  "created"?: Date;
  "modified"?: Date;
  contentEvents?: ContentEvent[];
  contentPages?: ContentPage[];
  contentProducts?: ContentProduct[];
  contentPosts?: ContentPost[];
  contentMultimediaModels?: any[];
  contentCategories?: ContentCategory[];
  contentEntities?: ContentEntity[];
  contentCards?: ContentCard[];
  contentTags?: ContentTag[];
  contentArticles?: ContentArticle[];
  storageFiles?: StorageFile[];
  contents?: SystemContent[];
}

export class SystemDomain implements SystemDomainInterface {
  "id": string;
  "name": string;
  "email": string;
  "created": Date;
  "modified": Date;
  contentEvents: ContentEvent[];
  contentPages: ContentPage[];
  contentProducts: ContentProduct[];
  contentPosts: ContentPost[];
  contentMultimediaModels: any[];
  contentCategories: ContentCategory[];
  contentEntities: ContentEntity[];
  contentCards: ContentCard[];
  contentTags: ContentTag[];
  contentArticles: ContentArticle[];
  storageFiles: StorageFile[];
  contents: SystemContent[];
  constructor(data?: SystemDomainInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `SystemDomain`.
   */
  public static getModelName() {
    return "SystemDomain";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of SystemDomain for dynamic purposes.
  **/
  public static factory(data: SystemDomainInterface): SystemDomain{
    return new SystemDomain(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'SystemDomain',
      plural: 'Domains',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "created": {
          name: 'created',
          type: 'Date'
        },
        "modified": {
          name: 'modified',
          type: 'Date'
        },
      },
      relations: {
        contentEvents: {
          name: 'contentEvents',
          type: 'ContentEvent[]',
          model: 'ContentEvent'
        },
        contentPages: {
          name: 'contentPages',
          type: 'ContentPage[]',
          model: 'ContentPage'
        },
        contentProducts: {
          name: 'contentProducts',
          type: 'ContentProduct[]',
          model: 'ContentProduct'
        },
        contentPosts: {
          name: 'contentPosts',
          type: 'ContentPost[]',
          model: 'ContentPost'
        },
        contentMultimediaModels: {
          name: 'contentMultimediaModels',
          type: 'any[]',
          model: ''
        },
        contentCategories: {
          name: 'contentCategories',
          type: 'ContentCategory[]',
          model: 'ContentCategory'
        },
        contentEntities: {
          name: 'contentEntities',
          type: 'ContentEntity[]',
          model: 'ContentEntity'
        },
        contentCards: {
          name: 'contentCards',
          type: 'ContentCard[]',
          model: 'ContentCard'
        },
        contentTags: {
          name: 'contentTags',
          type: 'ContentTag[]',
          model: 'ContentTag'
        },
        contentArticles: {
          name: 'contentArticles',
          type: 'ContentArticle[]',
          model: 'ContentArticle'
        },
        storageFiles: {
          name: 'storageFiles',
          type: 'StorageFile[]',
          model: 'StorageFile'
        },
        contents: {
          name: 'contents',
          type: 'SystemContent[]',
          model: 'SystemContent'
        },
      }
    }
  }
}
