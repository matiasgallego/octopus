/* tslint:disable */
import {
  ContentCategory,
  ContentEntity,
  ContentCard,
  StorageFile,
  SystemDomain,
  SystemUser
} from '../index';

declare var Object: any;
export interface ContentTagInterface {
  "name": string;
  "description"?: string;
  "id"?: number;
  "systemDomainId"?: string;
  "systemUserId"?: string;
  "storageFileId"?: string;
  "created"?: Date;
  "modified"?: Date;
  ContentCategories?: ContentCategory[];
  ContentEntities?: ContentEntity[];
  ContentCards?: ContentCard[];
  storageFile?: StorageFile;
  systemDomain?: SystemDomain;
  systemUser?: SystemUser;
}

export class ContentTag implements ContentTagInterface {
  "name": string;
  "description": string;
  "id": number;
  "systemDomainId": string;
  "systemUserId": string;
  "storageFileId": string;
  "created": Date;
  "modified": Date;
  ContentCategories: ContentCategory[];
  ContentEntities: ContentEntity[];
  ContentCards: ContentCard[];
  storageFile: StorageFile;
  systemDomain: SystemDomain;
  systemUser: SystemUser;
  constructor(data?: ContentTagInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `ContentTag`.
   */
  public static getModelName() {
    return "ContentTag";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of ContentTag for dynamic purposes.
  **/
  public static factory(data: ContentTagInterface): ContentTag{
    return new ContentTag(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'ContentTag',
      plural: 'ContentTags',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "systemDomainId": {
          name: 'systemDomainId',
          type: 'string'
        },
        "systemUserId": {
          name: 'systemUserId',
          type: 'string'
        },
        "storageFileId": {
          name: 'storageFileId',
          type: 'string'
        },
        "created": {
          name: 'created',
          type: 'Date'
        },
        "modified": {
          name: 'modified',
          type: 'Date'
        },
      },
      relations: {
        ContentCategories: {
          name: 'ContentCategories',
          type: 'ContentCategory[]',
          model: 'ContentCategory'
        },
        ContentEntities: {
          name: 'ContentEntities',
          type: 'ContentEntity[]',
          model: 'ContentEntity'
        },
        ContentCards: {
          name: 'ContentCards',
          type: 'ContentCard[]',
          model: 'ContentCard'
        },
        storageFile: {
          name: 'storageFile',
          type: 'StorageFile',
          model: 'StorageFile'
        },
        systemDomain: {
          name: 'systemDomain',
          type: 'SystemDomain',
          model: 'SystemDomain'
        },
        systemUser: {
          name: 'systemUser',
          type: 'SystemUser',
          model: 'SystemUser'
        },
      }
    }
  }
}
