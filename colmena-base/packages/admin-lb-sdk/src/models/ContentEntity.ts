/* tslint:disable */
import {
  ContentCategory,
  ContentTag,
  StorageFile,
  SystemDomain,
  SystemUser,
  GeoPoint
} from '../index';

declare var Object: any;
export interface ContentEntityInterface {
  "enabled"?: boolean;
  "name"?: string;
  "title"?: string;
  "description"?: string;
  "matterportModelId"?: string;
  "geopoint"?: GeoPoint;
  "address"?: any;
  "created"?: Date;
  "thumbnail"?: string;
  "url"?: string;
  "images"?: any;
  "panorama"?: any;
  "videos"?: any;
  "slideshow"?: string;
  "photopack"?: string;
  "panopack"?: string;
  "html"?: string;
  "image"?: string;
  "media"?: string;
  "file"?: string;
  "id"?: number;
  "systemDomainId"?: string;
  "systemUserId"?: string;
  "storageFileId"?: string;
  "modified"?: Date;
  ContentCategories?: ContentCategory[];
  ContentTags?: ContentTag[];
  storageFile?: StorageFile;
  systemDomain?: SystemDomain;
  systemUser?: SystemUser;
}

export class ContentEntity implements ContentEntityInterface {
  "enabled": boolean;
  "name": string;
  "title": string;
  "description": string;
  "matterportModelId": string;
  "geopoint": GeoPoint;
  "address": any;
  "created": Date;
  "thumbnail": string;
  "url": string;
  "images": any;
  "panorama": any;
  "videos": any;
  "slideshow": string;
  "photopack": string;
  "panopack": string;
  "html": string;
  "image": string;
  "media": string;
  "file": string;
  "id": number;
  "systemDomainId": string;
  "systemUserId": string;
  "storageFileId": string;
  "modified": Date;
  ContentCategories: ContentCategory[];
  ContentTags: ContentTag[];
  storageFile: StorageFile;
  systemDomain: SystemDomain;
  systemUser: SystemUser;
  constructor(data?: ContentEntityInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `ContentEntity`.
   */
  public static getModelName() {
    return "ContentEntity";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of ContentEntity for dynamic purposes.
  **/
  public static factory(data: ContentEntityInterface): ContentEntity{
    return new ContentEntity(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'ContentEntity',
      plural: 'ContentEntities',
      properties: {
        "enabled": {
          name: 'enabled',
          type: 'boolean',
          default: true
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "title": {
          name: 'title',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "matterportModelId": {
          name: 'matterportModelId',
          type: 'string'
        },
        "geopoint": {
          name: 'geopoint',
          type: 'GeoPoint'
        },
        "address": {
          name: 'address',
          type: 'any'
        },
        "created": {
          name: 'created',
          type: 'Date'
        },
        "thumbnail": {
          name: 'thumbnail',
          type: 'string'
        },
        "url": {
          name: 'url',
          type: 'string'
        },
        "images": {
          name: 'images',
          type: 'any'
        },
        "panorama": {
          name: 'panorama',
          type: 'any'
        },
        "videos": {
          name: 'videos',
          type: 'any'
        },
        "slideshow": {
          name: 'slideshow',
          type: 'string'
        },
        "photopack": {
          name: 'photopack',
          type: 'string'
        },
        "panopack": {
          name: 'panopack',
          type: 'string'
        },
        "html": {
          name: 'html',
          type: 'string'
        },
        "image": {
          name: 'image',
          type: 'string'
        },
        "media": {
          name: 'media',
          type: 'string'
        },
        "file": {
          name: 'file',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "systemDomainId": {
          name: 'systemDomainId',
          type: 'string'
        },
        "systemUserId": {
          name: 'systemUserId',
          type: 'string'
        },
        "storageFileId": {
          name: 'storageFileId',
          type: 'string'
        },
        "modified": {
          name: 'modified',
          type: 'Date'
        },
      },
      relations: {
        ContentCategories: {
          name: 'ContentCategories',
          type: 'ContentCategory[]',
          model: 'ContentCategory'
        },
        ContentTags: {
          name: 'ContentTags',
          type: 'ContentTag[]',
          model: 'ContentTag'
        },
        storageFile: {
          name: 'storageFile',
          type: 'StorageFile',
          model: 'StorageFile'
        },
        systemDomain: {
          name: 'systemDomain',
          type: 'SystemDomain',
          model: 'SystemDomain'
        },
        systemUser: {
          name: 'systemUser',
          type: 'SystemUser',
          model: 'SystemUser'
        },
      }
    }
  }
}
