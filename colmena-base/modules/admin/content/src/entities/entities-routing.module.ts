import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { EntityDetailComponent } from './components/entity-detail.component'
import { EntityFormComponent } from './components/entity-form.component'
import { EntityListComponent } from './components/entity-list.component'

import { EntitiesResolver } from './entities.resolvers'

const routes: Routes = [
  {
    path: '',
    data: { title: 'Entities' },
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: EntityListComponent,
        data: { title: 'List' },
      },
      {
        path: 'create',
        component: EntityDetailComponent,
        data: { title: 'Create' },
        children: [
          {
            path: '',
            component: EntityFormComponent,
          },
        ],
      },
      {
        path: ':id',
        component: EntityDetailComponent,
        resolve: {
          entity: EntitiesResolver,
        },
        data: { title: 'Entity' },
        children: [
          { path: '', redirectTo: 'edit', pathMatch: 'full' },
          {
            path: 'edit',
            component: EntityFormComponent,
            data: { title: 'Edit' },
          },
        ],
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntitiesRoutingModule {}
