import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-content-entity',
  template: `
    <ui-card>
      <ui-card-header>
        <i class="icon-folder"></i>
        {{item.title}}
      </ui-card-header>
      <ui-card-content>
        <p *ngIf="item.storageFile?.url"><img src="{{item.storageFile?.url}}" class="img-fluid" ></p>
        <div *ngIf="item.description" [innerHtml]="item.description"></div>
        <img *ngIf="item.image" [src]="item.image" style="width:100%;margin-top:10px"/>
        <!--<div *ngIf="item.address"><strong>Address: </strong>{{item.address}}</div>-->
      </ui-card-content>
      <ui-card-footer>
        <div *ngIf="item.created"><strong>Created at: </strong>{{item.created}}</div>
      </ui-card-footer>
    </ui-card>
  `,
})
export class EntityComponent {

  @Input() item: any = {}

}
