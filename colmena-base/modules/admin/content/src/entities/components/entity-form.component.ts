import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { UiService } from '@colmena/admin-ui'

import { ContentEntity, EntitiesService } from '../entities.service'

import {RequestOptions, Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-entity-form',
  template: `
  
  <div class="row">
    <div class="col-md-6">
      <ui-form *ngIf="item" [config]="formConfig" [item]="item" (action)="handleAction($event)"></ui-form>
    </div>
    <div class="col-md-6">
      <app-content-entity [item]="item"></app-content-entity>
      
      <ui-card>
        <ui-card-header>Matterport</ui-card-header>
        <ui-card-content>
          
          <div class="form-group">
            <label for="systemUserId">System User ID</label>
            <input id="systemUserId" type="text" class="form-control" [(ngModel)]="systemUserId" />
            
            <label for="matterportModelId">Matterport Model ID</label>
            <input id="matterportModelId" type="text" class="form-control" [(ngModel)]="matterportModelId" />
          </div>
          
          <button *ngIf="!service.selectedEntity" type="button" class="btn btn-primary" (click)="importFromMatterport()">Importar</button>
          <button *ngIf="service.selectedEntity" type="button" class="btn btn-primary" (click)="updateFromMatterport()">Actualizar</button>
    
        </ui-card-content>
      </ui-card>

      <ui-card *ngIf="service.selectedEntity">
        <ui-card-header>Aditional Images (floorplans)</ui-card-header>
        <ui-card-content>
          <div class="form-group">
            <p *ngFor="let floorplan of item.floorplans" style="position: relative;text-align: center;">
              <img src="{{floorplan}}" class="img-fluid">
              <button class="btn btn-clear" type="button" (click)="removeFloorplan(floorplan)" style="position: absolute;right: 5px;top: 5px;border-radius: 35%;">
                <i class="icon-trash"></i>
              </button>
            </p>
          </div>

          <label for="files" class="btn btn-primary">Subir un Plano</label>
          <input id="files" style="display:none;" type="file" (change)="onFileChange($event)">
        </ui-card-content>
      </ui-card>

    </div>
  </div>
  `,
})
export class EntityFormComponent implements OnInit {
  public formConfig: any = {}
  public item: any
  public matterportModelId: string = ''
  public systemUserId: string = ''

  public containerUrl

  constructor(
    private service: EntitiesService,
    private uiService: UiService,
    private router: Router,
    private http: Http
  ) {}

  ngOnInit() {
    this.item = this.service.selectedEntity || new ContentEntity()
    this.formConfig = this.service.getFormConfig()
    this.matterportModelId = this.item.matterportModelId
    this.systemUserId = this.item.systemUserId
    this.containerUrl = this.service.getContainerUrl(this.matterportModelId)
  }

  importFromMatterport() {
    return this.service.importMatterportModel(
      this.matterportModelId,
      this.systemUserId,
      () => {
        this.uiService.toastSuccess(
          'Import Entity Success',
          `<u>${this.item.name}</u> has done successfully`
        )
        this.handleAction({ action: 'cancel' })
      },
      err => this.uiService.toastError('Import Entity Fail', err.message)
    )
  }

  updateFromMatterport() {
    return this.service.updateMatterportModel(
      this.item,
      this.matterportModelId,
      () => {
        this.uiService.toastSuccess(
          'Update Entity Success',
          `<u>${this.item.name}</u> has done successfully`
        )
        this.handleAction({ action: 'cancel' })
      },
      err => this.uiService.toastError('Update Entity Fail', err.message)
    )
  }

  onFileChange(event: any) {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
      let file: File = fileList[0];
    
      let formData:FormData = new FormData();
      formData.append('uploadFile', file, file.name);
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      let options = new RequestOptions({ headers: headers });
      
      this.http.post(this.containerUrl + '/upload', formData, options)
          .map(res => res.json())
          .catch(error => Observable.throw(error))
          .subscribe(
              data => {
                if(!this.item.floorplans) this.item.floorplans = []
                this.item.floorplans.push(this.containerUrl + '/download/' + file.name)
                this.handleAction({ action: 'save', item: this.item, stay: true })
              },
              error => console.log(error)
          )
    }
  }

  removeFloorplan(image) {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Authorization', JSON.parse(localStorage.getItem('token')).id)
    let options = new RequestOptions({ headers: headers });

    let imageUrl = image.replace('download','files')
    this.http.delete(imageUrl, options)
      .map(res => res.json())
      .catch(error => Observable.throw(error))
      .subscribe(
          data => {
            this.item.floorplans.splice(image)
            this.handleAction({ action: 'save', item: this.item, stay: true })
          },
          error => console.log(error)
      )
  }

  handleAction(event) {
    switch (event.action) {
      case 'edit':
        return this.router.navigate([event.item.id])
        // return this.router.navigate(['/content/entities/' + event.object.id + '/edit'])
      case 'save':
        return this.service.upsertItem(
          event.item,
          () => {
            this.uiService.toastSuccess(
              'Save Entity Success',
              `<u>${event.item.name}</u> has been saved successfully`
            )
            if(!event.stay) this.handleAction({ action: 'cancel' })
          },
          err => this.uiService.toastError('Save Entity Fail', err.message)
        )
      case 'cancel':
        return this.router.navigate(['/content/entities'])
      default:
        return console.log('Unknown Event Action:', event)
    }
  }
}
