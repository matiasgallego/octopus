import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { UiTabLink } from '@colmena/admin-ui'

import { EntitiesService } from '../entities.service'

@Component({
  selector: 'app-entity-detail',
  template: `
    <ui-page [tabs]="tabs" [title]="item ? item.title : 'Add New Entity'">
      <router-outlet></router-outlet>
    </ui-page>
  `,
})
export class EntityDetailComponent implements OnInit {
  public tabs: UiTabLink[] = [
    { icon: 'fa fa-pencil', title: 'Edit', link: 'edit' },
  ]

  public item: any

  constructor(private service: EntitiesService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.item = this.route.snapshot.data.entity

    if (!this.item) {
      this.tabs = [{ icon: 'fa fa-plus', title: 'Create', link: '' }]
    }
    this.service.setSelectedEntity(this.item)
  }
}
