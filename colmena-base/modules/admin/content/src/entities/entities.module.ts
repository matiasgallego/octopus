import { NgModule } from '@angular/core'
import { ColmenaUiModule } from '@colmena/admin-ui'

import { FormsModule } from '@angular/forms'

import { EntitiesRoutingModule } from './entities-routing.module'

import { EntitiesService } from './entities.service'
import { EntitiesResolver } from './entities.resolvers'

import { EntityComponent } from './components/entity.component'
import { EntityDetailComponent } from './components/entity-detail.component'
import { EntityFormComponent } from './components/entity-form.component'
import { EntityListComponent } from './components/entity-list.component'

import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    ColmenaUiModule,
    EntitiesRoutingModule,
    FormsModule,
    HttpModule
  ],
  declarations: [
    EntityComponent,
    EntityDetailComponent,
    EntityFormComponent,
    EntityListComponent,
  ],
  providers: [
    EntitiesService,
    EntitiesResolver,
  ],
})
export class EntitiesModule {}
