import { Injectable } from '@angular/core'
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router'
import { Observable } from 'rxjs/Observable'

import { ContentEntity, EntitiesService } from './entities.service'

@Injectable()
export class EntitiesResolver implements Resolve<ContentEntity> {
  constructor(private service: EntitiesService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ContentEntity> {
    return this.service.getItem(route.params.id)
  }
}
