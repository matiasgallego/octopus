import { Injectable } from '@angular/core'
import { ContentEntity, SystemDomainApi, SystemUser, ContentEntityApi } from '@colmena/admin-lb-sdk'
export { ContentEntity } from '@colmena/admin-lb-sdk'
import { UiDataGridService, FormService } from '@colmena/admin-ui'
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription'

@Injectable()
export class EntitiesService extends UiDataGridService {
  public icon = 'icon-folder'
  public title = 'Entities'
  public files: any[] = []
  public categories: any[] = []
  public user: SystemUser
  public selectedEntity: ContentEntity

  public tableColumns = [
    { field: 'title', label: 'Name', action: 'edit' },
    { field: 'description', label: 'Description' },
  ]

  constructor(
    private domainApi: SystemDomainApi,
    private formService: FormService,
    private entityApi: ContentEntityApi
  ) {
    super()
    this.columns = this.tableColumns
  }

  setSelectedEntity(entity: ContentEntity) {
    this.selectedEntity = entity
  }

  getFormFields() {
    return [
      this.formService.select('categoryId', {
        label: 'Category',
        options: this.categories
      }),
      this.formService.input('systemUser.fullName', {
        label: 'User',
        placeholder: 'User Name',
      }),
      this.formService.input('title', {
        label: 'Name',
        placeholder: 'Entity name',
      }),
      this.formService.textarea('description', {
        label: 'Description',
        placeholder: 'Description text for the Entity',
      }),
      this.formService.wysiwyg('html', {
        label: 'HTML Entity body',
        placeholder: '',
      }),
      this.formService.input('image', {
        label: 'Image URL',
        placeholder: 'http://imageserver.com/your_image',
      }),
      this.formService.input('media', {
        label: 'Media URL',
        placeholder: 'https://my.matterport.com/show/?m=matterport_model_id',
      }),
      this.formService.input('slideshow', {
        label: 'Video URL',
        placeholder: 'http://www.youtube.com/your_video',
      })
    ]
  }

  // this.formService.select('storageFileId', {
  //   label: 'File',
  //   options: this.files,
  // })

  getFormConfig() {
    return {
      icon: this.icon,
      fields: this.getFormFields(),
      showCancel: true,
      hasHeader: false,
    }
  }

  getFiles() {
    this.domainApi
      .getStorageFiles(this.domain.id)
      .subscribe(files =>
        files.map(file => this.files.push({ value: file.id, label: file.name }))
      )
  }

  getContainerUrl(model_id) {
    const apiConfig = JSON.parse(window.localStorage.getItem('apiConfig'))
    return [ apiConfig.baseUrl, apiConfig.version, 'StorageContainers', 'model_' + model_id ].join('/')
  }

  getCategories() {
    this.domainApi
      .getContentCategories(this.domain.id)
      .subscribe(categories =>
        categories.map(category => this.categories.push({ value: category.id, label: category.title }))
      )
  }

  getItems(): Observable<ContentEntity[]> {
    return this.domainApi.getContentEntities(
      this.domain.id,
      this.getFilters({ include: ['storageFile', 'ContentCategories'] })
    )
  }

  getItem(id): Observable<ContentEntity> {
    return this.domainApi.findByIdContentEntities(this.domain.id, id)
  }

  getItemCount(): Observable<any> {
    return this.domainApi.countContentEntities(
      this.domain.id,
      this.getWhereFilters()
    )
  }

  upsertItem(item, successCb, errorCb): Subscription {
    if (item.id) {
      return this.upsertEntity(item, successCb, errorCb)
    }
    return this.createEntity(item, successCb, errorCb)
  }

  upsertEntity(item, successCb, errorCb): Subscription {
    return this.domainApi
      .updateByIdContentEntities(this.domain.id, item.id, item)
      .subscribe(successCb, errorCb)
  }

  createEntity(item, successCb, errorCb): Subscription {
    return this.domainApi
      .createContentEntities(this.domain.id, item)
      .subscribe(successCb, errorCb)
  }

  deleteItem(item, successCb, errorCb): Subscription {
    return this.domainApi
      .destroyByIdContentEntities(this.domain.id, item.id)
      .subscribe(successCb, errorCb)
  }

  updateMatterportModel(item, matterportModelId, successCb, errorCb): Subscription {
    return this.entityApi
      .importFromMatterportAPI(item.id, item.systemUserId, matterportModelId, null, null)
      .subscribe(successCb, errorCb)
  }

  importMatterportModel(matterportModelId, systemUserId, successCb, errorCb): Subscription {
    return this.entityApi
      .importFromMatterportAPI(null, systemUserId, matterportModelId, null, null)
      .subscribe(successCb, errorCb)
  }

}
