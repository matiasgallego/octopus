import { Injectable } from '@angular/core'
import { ContentArticle, SystemDomainApi, SystemUser, ContentArticleApi } from '@colmena/admin-lb-sdk'
export { ContentArticle } from '@colmena/admin-lb-sdk'
import { UiDataGridService, FormService } from '@colmena/admin-ui'
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription'

@Injectable()
export class ArticlesService extends UiDataGridService {
  public icon = 'icon-folder'
  public title = 'Articles'
  public files: any[] = []
  public categories: any[] = []
  public user: SystemUser
  public selectedArticle: ContentArticle

  public tableColumns = [
    { field: 'author', label: 'Author', action: 'edit' },
    { field: 'entry', label: 'Entry' },
  ]

  constructor(
    private domainApi: SystemDomainApi,
    private formService: FormService,
    private articleApi: ContentArticleApi
  ) {
    super()
    this.columns = this.tableColumns
  }

  setSelectedArticle(article: ContentArticle) {
    this.selectedArticle = article
  }

  getFormFields() {
    return [
      this.formService.input('author', {
        label: 'Author Name'
      }),
      this.formService.input('author_url', {
        label: 'Author url'
      }),
      this.formService.input('entry', {
        label: 'Entry'
      }),
      this.formService.input('entry_url', {
        label: 'Entry url'
      }),
      this.formService.input('date', {
        label: 'Date'
      }),
      this.formService.input('location', {
        label: 'Location'
      }),
      this.formService.input('location_url', {
        label: 'Location url'
      })
    ]
  }

  getFormConfig() {
    return {
      icon: this.icon,
      fields: this.getFormFields(),
      showCancel: true,
      hasHeader: false,
    }
  }

  getItems(): Observable<ContentArticle[]> {
    return this.domainApi.getContentArticles(
      this.domain.id,
      this.getFilters()
    )
  }

  getItem(id): Observable<ContentArticle> {
    return this.domainApi.findByIdContentArticles(this.domain.id, id)
  }

  getItemCount(): Observable<any> {
    return this.domainApi.countContentArticles(
      this.domain.id,
      this.getWhereFilters()
    )
  }

  upsertItem(item, successCb, errorCb): Subscription {
    if (item.id) {
      return this.upsertArticle(item, successCb, errorCb)
    }
    return this.createArticle(item, successCb, errorCb)
  }

  upsertArticle(item, successCb, errorCb): Subscription {
    return this.domainApi
      .updateByIdContentArticles(this.domain.id, item.id, item)
      .subscribe(successCb, errorCb)
  }

  createArticle(item, successCb, errorCb): Subscription {
    return this.domainApi
      .createContentArticles(this.domain.id, item)
      .subscribe(successCb, errorCb)
  }

  deleteItem(item, successCb, errorCb): Subscription {
    return this.domainApi
      .destroyByIdContentArticles(this.domain.id, item.id)
      .subscribe(successCb, errorCb)
  }

  importParsehubRun(runToken, apiKey, domainId, successCb, errorCb): Subscription {
    return this.articleApi
      .getParsehubRun(runToken, apiKey, domainId, null)
      .subscribe(successCb, errorCb)
  }

}
