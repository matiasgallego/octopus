import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { ArticleDetailComponent } from './components/article-detail.component'
import { ArticleFormComponent } from './components/article-form.component'
import { ArticleListComponent } from './components/article-list.component'
import { ArticleImportComponent } from './components/article-import.component'

import { ArticlesResolver } from './articles.resolvers'

const routes: Routes = [
  {
    path: '',
    data: { title: 'Articles' },
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: ArticleListComponent,
        data: { title: 'List' },
      },
      {
        path: 'create',
        component: ArticleDetailComponent,
        data: { title: 'Create' },
        children: [
          {
            path: '',
            component: ArticleFormComponent,
          },
          {
            path: 'import',
            component: ArticleImportComponent,
            data: { title: 'Password' },
          },
        ],
      },
      {
        path: ':id',
        component: ArticleDetailComponent,
        resolve: {
          article: ArticlesResolver,
        },
        data: { title: 'Article' },
        children: [
          { path: '', redirectTo: 'edit', pathMatch: 'full' },
          {
            path: 'edit',
            component: ArticleFormComponent,
            data: { title: 'Edit' },
          },
        ],
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArticlesRoutingModule {}
