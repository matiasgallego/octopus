import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-content-article',
  template: `
    <ui-card>
      <ui-card-header>
        <i class="icon-folder"></i>
        {{item.title}}
      </ui-card-header>
      <ui-card-content>
        
      </ui-card-content>
      <ui-card-footer>
        <div *ngIf="item.created"><strong>Created at: </strong>{{item.created}}</div>
      </ui-card-footer>
    </ui-card>
  `,
})
export class ArticleComponent {

  @Input() item: any = {}

}
