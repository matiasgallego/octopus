import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { UiService, FormService } from '@colmena/admin-ui'
import { ArticlesService } from '../articles.service'

@Component({
  selector: 'app-article-import',
  template: `
    <div class="row">
        <div class="col">
        <ui-card>
            <ui-card-header>ParseHub</ui-card-header>
            <ui-card-content>
            
            <div class="form-group">
                <label for="runToken">Run Token</label>
                <input id="runToken" type="text" class="form-control" [(ngModel)]="runToken" />
                
                <label for="apiKey">API Key</label>
                <input id="apiKey" type="text" class="form-control" [(ngModel)]="apiKey" />
            </div>
            
            <button *ngIf="!service.selectedArticle" type="button" class="btn btn-primary" (click)="importFromParsehub()">Importar</button>
            
            </ui-card-content>
        </ui-card>
        </div>
    </div>
  `
})

export class ArticleImportComponent implements OnInit {

  public runToken: string = ''
  public apiKey: string = ''

  public formConfig: any = {
    hasHeader: false,
    fields: [],
    showCancel: true,
  }

  public item: any

  constructor(
    public service: ArticlesService,
    public uiService: UiService,
    private formService: FormService,
    private router: Router,
  ) {
  }

  ngOnInit() {
  }

  importFromParsehub() {
    return this.service.importParsehubRun(
      this.runToken,
      this.apiKey,
      JSON.parse(window.localStorage.getItem('domain')).id,
      (data) => {
        console.log(data);
        this.uiService.toastSuccess(
          'Import from ParseHub Service Success',
          `<u>${data.result}</u> records imported successfully`
        )
        this.handleAction({ action: 'cancel' })
      },
      err => this.uiService.toastError('Import from ParseHub Service Fail with Error', err.message)
    )
  }

  handleAction(event) {
    switch (event.action) {
      case 'edit':
        return this.router.navigate([event.item.id])
        // return this.router.navigate(['/content/articles/' + event.object.id + '/edit'])
      case 'save':
        return this.service.upsertItem(
          event.item,
          () => {
            this.uiService.toastSuccess(
              'Save Article Success',
              `Imported from Parsehub successfully`
            )
            if(!event.stay) this.handleAction({ action: 'cancel' })
          },
          err => this.uiService.toastError('Save Article Fail', err.message)
        )
      case 'cancel':
        return this.router.navigate(['/content/articles'])
      default:
        return console.log('Unknown Event Action:', event)
    }
  }

}
