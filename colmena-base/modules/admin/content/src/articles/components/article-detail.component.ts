import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { UiTabLink } from '@colmena/admin-ui'

import { ArticlesService } from '../articles.service'

@Component({
  selector: 'app-article-detail',
  template: `
    <ui-page [tabs]="tabs" [title]="item ? item.title : 'Add New Article'">
      <router-outlet></router-outlet>
    </ui-page>
  `,
})
export class ArticleDetailComponent implements OnInit {
  public tabs: UiTabLink[] = [
    { icon: 'fa fa-pencil', title: 'Edit', link: 'edit' },
  ]

  public item: any

  constructor(private service: ArticlesService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.item = this.route.snapshot.data.article

    if (!this.item) {
      this.tabs = [
        { icon: 'fa fa-plus', title: 'Create', link: '' },
        { icon: 'fa fa-cloud-download', title: 'Import from ParseHub', link: 'import' }
      ]
    }
    this.service.setSelectedArticle(this.item)
  }
}
