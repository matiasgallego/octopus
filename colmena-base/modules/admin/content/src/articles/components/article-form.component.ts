import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { UiService } from '@colmena/admin-ui'

import { ContentArticle, ArticlesService } from '../articles.service'

import {RequestOptions, Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-article-form',
  template: `
  
  <div class="row">
    <div class="col">
      <ui-form *ngIf="item" [config]="formConfig" [item]="item" (action)="handleAction($event)"></ui-form>
    </div>
  </div>
  `,
})
export class ArticleFormComponent implements OnInit {
  public formConfig: any = {}
  public item: any

  constructor(
    private service: ArticlesService,
    private uiService: UiService,
    private router: Router,
    private http: Http
  ) {}

  ngOnInit() {
    this.item = this.service.selectedArticle || new ContentArticle()
    this.formConfig = this.service.getFormConfig()
  }

  handleAction(event) {
    switch (event.action) {
      case 'edit':
        return this.router.navigate([event.item.id])
        // return this.router.navigate(['/content/articles/' + event.object.id + '/edit'])
      case 'save':
        return this.service.upsertItem(
          event.item,
          () => {
            this.uiService.toastSuccess(
              'Save Article Success',
              `<u>${event.item.name}</u> has been saved successfully`
            )
            if(!event.stay) this.handleAction({ action: 'cancel' })
          },
          err => this.uiService.toastError('Save Article Fail', err.message)
        )
      case 'cancel':
        return this.router.navigate(['/content/articles'])
      default:
        return console.log('Unknown Event Action:', event)
    }
  }
}
