import { Injectable } from '@angular/core'
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router'
import { Observable } from 'rxjs/Observable'

import { ContentArticle, ArticlesService } from './articles.service'

@Injectable()
export class ArticlesResolver implements Resolve<ContentArticle> {
  constructor(private service: ArticlesService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ContentArticle> {
    return this.service.getItem(route.params.id)
  }
}
