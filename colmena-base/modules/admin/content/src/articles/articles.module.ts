import { NgModule } from '@angular/core'
import { ColmenaUiModule } from '@colmena/admin-ui'

import { FormsModule } from '@angular/forms'

import { ArticlesRoutingModule } from './articles-routing.module'

import { ArticlesService } from './articles.service'
import { ArticlesResolver } from './articles.resolvers'

import { ArticleComponent } from './components/article.component'
import { ArticleDetailComponent } from './components/article-detail.component'
import { ArticleFormComponent } from './components/article-form.component'
import { ArticleListComponent } from './components/article-list.component'
import { ArticleImportComponent } from './components/article-import.component'

import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    ColmenaUiModule,
    ArticlesRoutingModule,
    FormsModule,
    HttpModule
  ],
  declarations: [
    ArticleComponent,
    ArticleDetailComponent,
    ArticleFormComponent,
    ArticleListComponent,
    ArticleImportComponent,
  ],
  providers: [
    ArticlesService,
    ArticlesResolver,
  ],
})
export class ArticlesModule {}
