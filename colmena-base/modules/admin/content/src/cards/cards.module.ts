import { NgModule } from '@angular/core'
import { ColmenaUiModule } from '@colmena/admin-ui'

import { CardsRoutingModule } from './cards-routing.module'

import { CardsService } from './cards.service'
import { CardsResolver } from './cards.resolvers'

import { CardComponent } from './components/card.component'
import { CardDetailComponent } from './components/card-detail.component'
import { CardFormComponent } from './components/card-form.component'
import { CardListComponent } from './components/card-list.component'

@NgModule({
  imports: [
    ColmenaUiModule,
    CardsRoutingModule,
  ],
  declarations: [
    CardComponent,
    CardDetailComponent,
    CardFormComponent,
    CardListComponent,
  ],
  providers: [
    CardsService,
    CardsResolver,
  ],
})
export class CardsModule {}
