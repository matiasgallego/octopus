import { Injectable } from '@angular/core'
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router'
import { Observable } from 'rxjs/Observable'

import { ContentCard, CardsService } from './cards.service'

@Injectable()
export class CardsResolver implements Resolve<ContentCard> {
  constructor(private service: CardsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ContentCard> {
    return this.service.getItem(route.params.id)
  }
}
