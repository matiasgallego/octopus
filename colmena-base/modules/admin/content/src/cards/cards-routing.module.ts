import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { CardDetailComponent } from './components/card-detail.component'
import { CardFormComponent } from './components/card-form.component'
import { CardListComponent } from './components/card-list.component'

import { CardsResolver } from './cards.resolvers'

const routes: Routes = [
  {
    path: '',
    data: { title: 'Cards' },
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: CardListComponent,
        data: { title: 'List' },
      },
      {
        path: 'create',
        component: CardDetailComponent,
        data: { title: 'Create' },
        children: [
          {
            path: '',
            component: CardFormComponent,
          },
        ],
      },
      {
        path: ':id',
        component: CardDetailComponent,
        resolve: {
          card: CardsResolver,
        },
        data: { title: 'Card' },
        children: [
          { path: '', redirectTo: 'edit', pathMatch: 'full' },
          {
            path: 'edit',
            component: CardFormComponent,
            data: { title: 'Edit' },
          },
        ],
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardsRoutingModule {}
