import { Injectable } from '@angular/core'
import { ContentCard, SystemDomainApi } from '@colmena/admin-lb-sdk'
export { ContentCard } from '@colmena/admin-lb-sdk'
import { UiDataGridService, FormService } from '@colmena/admin-ui'
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription'

@Injectable()
export class CardsService extends UiDataGridService {
  public icon = 'icon-folder'
  public title = 'Cards'
  public files: any[] = []
  public selectedCard: ContentCard

  public tableColumns = [
    { field: 'title', label: 'Name', action: 'edit' },
    { field: 'description', label: 'Description' },
  ]

  constructor(
    private domainApi: SystemDomainApi,
    private formService: FormService
  ) {
    super()
    this.columns = this.tableColumns
  }

  setSelectedCard(card: ContentCard) {
    this.selectedCard = card
  }

  getFormFields() {
    return [
      this.formService.input('title', {
        label: 'Name',
        placeholder: 'Card name',
      }),
      this.formService.textarea('description', {
        label: 'Description',
        placeholder: 'Description text for the Card',
      }),
      this.formService.input('order', {
        label: 'Order Number',
        placeholder: 'Position in the list',
      }),
      this.formService.wysiwyg('html', {
        label: 'HTML Card body',
        placeholder: '',
      }),
      this.formService.input('image', {
        label: 'Image URL',
        placeholder: 'http://imageserver.com/your_image',
      }),
      this.formService.input('media', {
        label: 'Media URL',
        placeholder: 'http://www.youtube.com/your_video',
      }),

      this.formService.select('storageFileId', {
        label: 'File',
        options: this.files,
      })
    ]
  }

  getFormConfig() {
    return {
      icon: this.icon,
      fields: this.getFormFields(),
      showCancel: true,
      hasHeader: false,
    }
  }

  getFiles() {
    this.domainApi
      .getStorageFiles(this.domain.id)
      .subscribe(files =>
        files.map(file => this.files.push({ value: file.id, label: file.name }))
      )
  }

  getItems(): Observable<ContentCard[]> {
    return this.domainApi.getContentCards(
      this.domain.id,
      this.getFilters({ include: ['storageFile'] })
    )
  }

  getItem(id): Observable<ContentCard> {
    return this.domainApi.findByIdContentCards(this.domain.id, id)
  }

  getItemCount(): Observable<any> {
    return this.domainApi.countContentCards(
      this.domain.id,
      this.getWhereFilters()
    )
  }

  upsertItem(item, successCb, errorCb): Subscription {
    if (item.id) {
      return this.upsertCard(item, successCb, errorCb)
    }
    return this.createCard(item, successCb, errorCb)
  }

  upsertCard(item, successCb, errorCb): Subscription {
    return this.domainApi
      .updateByIdContentCards(this.domain.id, item.id, item)
      .subscribe(successCb, errorCb)
  }

  createCard(item, successCb, errorCb): Subscription {
    return this.domainApi
      .createContentCards(this.domain.id, item)
      .subscribe(successCb, errorCb)
  }

  deleteItem(item, successCb, errorCb): Subscription {
    return this.domainApi
      .destroyByIdContentCards(this.domain.id, item.id)
      .subscribe(successCb, errorCb)
  }
}
