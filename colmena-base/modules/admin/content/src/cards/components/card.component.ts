import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-content-card',
  template: `
    <ui-card>
      <ui-card-header>
        <i class="icon-folder"></i>
        {{item.title}}
      </ui-card-header>
      <ui-card-content>
        <p *ngIf="item.storageFile?.url"><img src="{{item.storageFile?.url}}" class="img-fluid" ></p>
        <div *ngIf="item.description" [innerHtml]="item.description"></div>
        <img *ngIf="item.image" [src]="item.image" style="width:100%;margin-top:10px"/>
      </ui-card-content>
      <ui-card-footer>
        <div *ngIf="item.created"><strong>Created at: </strong>{{item.created}}</div>
        <div *ngIf="item.order"><strong>Position in list: </strong>{{item.order}}</div>
      </ui-card-footer>
    </ui-card>
  `,
})
export class CardComponent {

  @Input() item: any = {}

}
