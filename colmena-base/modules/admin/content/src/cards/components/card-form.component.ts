import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { UiService } from '@colmena/admin-ui'

import { ContentCard, CardsService } from '../cards.service'

@Component({
  selector: 'app-card-form',
  template: `
  <div class="row">
    <div class="col-md-6">
      <ui-form *ngIf="item" [config]="formConfig" [item]="item" (action)="handleAction($event)"></ui-form>
    </div>
    <div class="col-md-6">
      <app-content-card [item]="item"></app-content-card>
    </div>
  </div>
  `,
})
export class CardFormComponent implements OnInit {
  public formConfig: any = {}
  public item: any

  constructor(
    private service: CardsService,
    private uiService: UiService,
    private router: Router
  ) {}

  ngOnInit() {
    this.item = this.service.selectedCard || new ContentCard()
    this.formConfig = this.service.getFormConfig()
  }

  handleAction(event) {
    switch (event.action) {
      case 'save':
        return this.service.upsertItem(
          event.item,
          () => {
            this.uiService.toastSuccess(
              'Save Card Success',
              `<u>${event.item.name}</u> has been saved successfully`
            )
            this.handleAction({ action: 'cancel' })
          },
          err => this.uiService.toastError('Save Card Fail', err.message)
        )
      case 'cancel':
        return this.router.navigate(['/content/cards'])
      default:
        return console.log('Unknown Event Action:', event)
    }
  }
}
