import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { UiTabLink } from '@colmena/admin-ui'

import { CardsService } from '../cards.service'

@Component({
  selector: 'app-card-detail',
  template: `
    <ui-page [tabs]="tabs" [title]="item ? item.title : 'Add New Card'">
      <router-outlet></router-outlet>
    </ui-page>
  `,
})
export class CardDetailComponent implements OnInit {
  public tabs: UiTabLink[] = [
    { icon: 'fa fa-pencil', title: 'Edit', link: 'edit' },
  ]

  public item: any

  constructor(private service: CardsService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.item = this.route.snapshot.data.card

    if (!this.item) {
      this.tabs = [{ icon: 'fa fa-plus', title: 'Create', link: '' }]
    }
    this.service.setSelectedCard(this.item)
  }
}
