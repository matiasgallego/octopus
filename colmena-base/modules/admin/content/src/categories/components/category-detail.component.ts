import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { UiTabLink } from '@colmena/admin-ui'

import { CategoriesService } from '../categories.service'

@Component({
  selector: 'app-category-detail',
  template: `
    <ui-page [tabs]="tabs" [title]="item ? item.title : 'Add New Category'">
      <router-outlet></router-outlet>
    </ui-page>
  `,
})
export class CategoryDetailComponent implements OnInit {
  public tabs: UiTabLink[] = [
    { icon: 'fa fa-pencil', title: 'Edit', link: 'edit' },
  ]

  public item: any

  constructor(private service: CategoriesService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.item = this.route.snapshot.data.category

    if (!this.item) {
      this.tabs = [{ icon: 'fa fa-plus', title: 'Create', link: '' }]
    }
    this.service.setSelectedCategory(this.item)
  }
}
