import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { UiService } from '@colmena/admin-ui'

import { ContentCategory, CategoriesService } from '../categories.service'

@Component({
  selector: 'app-category-form',
  template: `
  <div class="row">
    <div class="col-md-6">
      <ui-form *ngIf="item" [config]="formConfig" [item]="item" (action)="handleAction($event)"></ui-form>
    </div>
    <div class="col-md-6">
      <app-content-category [item]="item"></app-content-category>
    </div>
  </div>
  `,
})
export class CategoryFormComponent implements OnInit {
  public formConfig: any = {}
  public item: any

  constructor(
    private service: CategoriesService,
    private uiService: UiService,
    private router: Router
  ) {}

  ngOnInit() {
    this.item = this.service.selectedCategory || new ContentCategory()
    this.formConfig = this.service.getFormConfig()
  }

  handleAction(event) {
    switch (event.action) {
      case 'save':
        return this.service.upsertItem(
          event.item,
          () => {
            this.uiService.toastSuccess(
              'Save Category Success',
              `<u>${event.item.name}</u> has been saved successfully`
            )
            this.handleAction({ action: 'cancel' })
          },
          err => this.uiService.toastError('Save Category Fail', err.message)
        )
      case 'cancel':
        return this.router.navigate(['/content/categories'])
      default:
        return console.log('Unknown Event Action:', event)
    }
  }
}
