import { Injectable } from '@angular/core'
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router'
import { Observable } from 'rxjs/Observable'

import { ContentCategory, CategoriesService } from './categories.service'

@Injectable()
export class CategoriesResolver implements Resolve<ContentCategory> {
  constructor(private service: CategoriesService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ContentCategory> {
    return this.service.getItem(route.params.id)
  }
}
