import { NgModule } from '@angular/core'
import { ColmenaUiModule } from '@colmena/admin-ui'

import { CategoriesRoutingModule } from './categories-routing.module'

import { CategoriesService } from './categories.service'
import { CategoriesResolver } from './categories.resolvers'

import { CategoryComponent } from './components/category.component'
import { CategoryDetailComponent } from './components/category-detail.component'
import { CategoryFormComponent } from './components/category-form.component'
import { CategoryListComponent } from './components/category-list.component'

@NgModule({
  imports: [
    ColmenaUiModule,
    CategoriesRoutingModule,
  ],
  declarations: [
    CategoryComponent,
    CategoryDetailComponent,
    CategoryFormComponent,
    CategoryListComponent,
  ],
  providers: [
    CategoriesService,
    CategoriesResolver,
  ],
})
export class CategoriesModule {}
