import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { CategoryDetailComponent } from './components/category-detail.component'
import { CategoryFormComponent } from './components/category-form.component'
import { CategoryListComponent } from './components/category-list.component'

import { CategoriesResolver } from './categories.resolvers'

const routes: Routes = [
  {
    path: '',
    data: { title: 'Categories' },
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: CategoryListComponent,
        data: { title: 'List' },
      },
      {
        path: 'create',
        component: CategoryDetailComponent,
        data: { title: 'Create' },
        children: [
          {
            path: '',
            component: CategoryFormComponent,
          },
        ],
      },
      {
        path: ':id',
        component: CategoryDetailComponent,
        resolve: {
          category: CategoriesResolver,
        },
        data: { title: 'Category' },
        children: [
          { path: '', redirectTo: 'edit', pathMatch: 'full' },
          {
            path: 'edit',
            component: CategoryFormComponent,
            data: { title: 'Edit' },
          },
        ],
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriesRoutingModule {}
