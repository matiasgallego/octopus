import { Injectable } from '@angular/core'
import { ContentCategory, SystemDomainApi } from '@colmena/admin-lb-sdk'
export { ContentCategory } from '@colmena/admin-lb-sdk'
import { UiDataGridService, FormService } from '@colmena/admin-ui'
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription'

@Injectable()
export class CategoriesService extends UiDataGridService {
  public icon = 'icon-folder'
  public title = 'Categories'
  public files: any[] = []
  public selectedCategory: ContentCategory

  public tableColumns = [
    { field: 'title', label: 'Name', action: 'edit' },
    { field: 'description', label: 'Description' },
  ]

  constructor(
    private domainApi: SystemDomainApi,
    private formService: FormService
  ) {
    super()
    this.columns = this.tableColumns
  }

  setSelectedCategory(category: ContentCategory) {
    this.selectedCategory = category
  }

  getFormFields() {
    return [
      this.formService.input('title', {
        label: 'Name',
        placeholder: 'Category name',
      }),
      this.formService.textarea('description', {
        label: 'Description',
        placeholder: 'Description text for the Category',
      }),
      this.formService.input('order', {
        label: 'Order Number',
        placeholder: 'Position in the list',
      }),
      this.formService.wysiwyg('html', {
        label: 'HTML Category body',
        placeholder: '',
      }),
      this.formService.input('image', {
        label: 'Image URL',
        placeholder: 'http://imageserver.com/your_image',
      }),
      this.formService.input('media', {
        label: 'Media URL',
        placeholder: 'http://www.youtube.com/your_video',
      }),

      this.formService.select('storageFileId', {
        label: 'File',
        options: this.files,
      })
    ]
  }

  getFormConfig() {
    return {
      icon: this.icon,
      fields: this.getFormFields(),
      showCancel: true,
      hasHeader: false,
    }
  }

  getFiles() {
    this.domainApi
      .getStorageFiles(this.domain.id)
      .subscribe(files =>
        files.map(file => this.files.push({ value: file.id, label: file.name }))
      )
  }

  getItems(): Observable<ContentCategory[]> {
    return this.domainApi.getContentCategories(
      this.domain.id,
      this.getFilters({ include: ['storageFile'] })
    )
  }

  getItem(id): Observable<ContentCategory> {
    return this.domainApi.findByIdContentCategories(this.domain.id, id)
  }

  getItemCount(): Observable<any> {
    return this.domainApi.countContentCategories(
      this.domain.id,
      this.getWhereFilters()
    )
  }

  upsertItem(item, successCb, errorCb): Subscription {
    if (item.id) {
      return this.upsertCategory(item, successCb, errorCb)
    }
    return this.createCategory(item, successCb, errorCb)
  }

  upsertCategory(item, successCb, errorCb): Subscription {
    return this.domainApi
      .updateByIdContentCategories(this.domain.id, item.id, item)
      .subscribe(successCb, errorCb)
  }

  createCategory(item, successCb, errorCb): Subscription {
    return this.domainApi
      .createContentCategories(this.domain.id, item)
      .subscribe(successCb, errorCb)
  }

  deleteItem(item, successCb, errorCb): Subscription {
    return this.domainApi
      .destroyByIdContentCategories(this.domain.id, item.id)
      .subscribe(successCb, errorCb)
  }
}
