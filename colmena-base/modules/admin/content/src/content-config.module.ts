import { NgModule } from '@angular/core'
import { Store } from '@ngrx/store'

import { SystemDomainApi } from '@colmena/admin-lb-sdk'

const moduleName = 'content'

const link = (...links) => ([ '/', moduleName, ...links ])

let moduleConfig = {
  name: 'Content',
  icon: 'icon-layers',
  packageName: `@colmena/module-admin-${moduleName}`,
  topLinks: [
    { label: 'Content', icon: 'icon-layers', link: link() },
  ],
  sidebarLinks: [
    { weight: 5, type: 'title', name:'content', label: 'Content', active: 1 },
    { weight: 10, name: 'event', label: 'Events', icon: 'icon-event', link: link('events'), active: 0 },
    { weight: 30, name: 'page', label: 'Pages', icon: 'icon-book-open', link: link('pages'), active: 0 },
    { weight: 40, name: 'post', label: 'Posts', icon: 'icon-note', link: link('posts'), active: 0 },
    { weight: 50, name: 'product', label: 'Products', icon: 'icon-basket', link: link('products'), active: 0 },
    { weight: 60, name: 'category', label: 'Categories', icon: 'icon-event', link: link('categories'), active: 0 },
    { weight: 60, name: 'entity', label: 'Entities', icon: 'icon-docs', link: link('entities'), active: 0 },
    { weight: 60, name: 'card', label: 'Cards', icon: 'icon-grid', link: link('cards'), active: 0 },
    { weight: 60, name: 'tag', label: 'Tags', icon: 'icon-tag', link: link('tags'), active: 0 },
    { weight: 60, name: 'article', label: 'Articles', icon: 'icon-docs', link: link('articles'), active: 0 },
  ],
  dashboardLinks: {
    content: [
      { count: '∞', name: 'event', label: 'Events', type: 'info', icon: 'icon-event', link: link('events'), active: 0 },
      { count: '∞', name: 'page', label: 'Pages', type: 'primary', icon: 'icon-book-open', link: link('pages'), active: 0 },
      { count: '∞', name: 'post', label: 'Posts', type: 'warning', icon: 'icon-note', link: link('posts'), active: 0 },
      { count: '∞', name: 'product', label: 'Products', type: 'danger', icon: 'icon-basket', link: link('products'), active: 0 },
      { count: '∞', name: 'category', label: 'Categories', type: 'info', icon: 'icon-event', link: link('categories'), active: 0 },
      { count: '∞', name: 'entity', label: 'Entities', type: 'info', icon: 'icon-docs', link: link('entities'), active: 0 },
      { count: '∞', name: 'card', label: 'Cards', type: 'info', icon: 'icon-grid', link: link('cards'), active: 0 },
      { count: '∞', name: 'tag', label: 'Tags', type: 'info', icon: 'icon-tag', link: link('tags'), active: 0 },
      { count: '∞', name: 'article', label: 'Articles', type: 'primary', icon: 'icon-docs', link: link('articles'), active: 0 },
    ]
  },
}

@NgModule()
export class ContentConfigModule {
  currentDomain: { id: '', name: ''}
  constructor(protected store: Store<any>, private domainApi: SystemDomainApi) {
    
    if (window.localStorage.getItem('domain')) {
      this.currentDomain = JSON.parse(window.localStorage.getItem('domain'))
      domainApi.getContents(this.currentDomain.id).subscribe(
        res => {
          for (var y = 0; y < res.length; y++) {
            var element = res[y];
            for (var x = 0; x < moduleConfig.sidebarLinks.length; x++) {
              if(moduleConfig.sidebarLinks[x].name===element.name){
                moduleConfig.sidebarLinks[x].active=1
              }
            }
          }

          var i = moduleConfig.sidebarLinks.length
          while (--i) {
            if(moduleConfig.sidebarLinks[i].active==0){
              moduleConfig.sidebarLinks.splice(i,1)
              moduleConfig.dashboardLinks.content.splice(i,1)
            }
          }

          this.store.dispatch({ type: 'APP_LOAD_MODULE', payload: { moduleName, moduleConfig } })
        }
      )
    }
  }
}

