import { NgModule } from '@angular/core'
import { ColmenaUiModule } from '@colmena/admin-ui'

import { TagsRoutingModule } from './tags-routing.module'

import { TagsService } from './tags.service'
import { TagsResolver } from './tags.resolvers'

import { TagComponent } from './components/tag.component'
import { TagDetailComponent } from './components/tag-detail.component'
import { TagFormComponent } from './components/tag-form.component'
import { TagListComponent } from './components/tag-list.component'

@NgModule({
  imports: [
    ColmenaUiModule,
    TagsRoutingModule,
  ],
  declarations: [
    TagComponent,
    TagDetailComponent,
    TagFormComponent,
    TagListComponent,
  ],
  providers: [
    TagsService,
    TagsResolver,
  ],
})
export class TagsModule {}
