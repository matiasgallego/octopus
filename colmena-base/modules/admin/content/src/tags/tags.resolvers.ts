import { Injectable } from '@angular/core'
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router'
import { Observable } from 'rxjs/Observable'

import { ContentTag, TagsService } from './tags.service'

@Injectable()
export class TagsResolver implements Resolve<ContentTag> {
  constructor(private service: TagsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ContentTag> {
    return this.service.getItem(route.params.id)
  }
}
