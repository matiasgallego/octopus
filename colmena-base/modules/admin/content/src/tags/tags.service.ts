import { Injectable } from '@angular/core'
import { ContentTag, SystemDomainApi } from '@colmena/admin-lb-sdk'
export { ContentTag } from '@colmena/admin-lb-sdk'
import { UiDataGridService, FormService } from '@colmena/admin-ui'
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription'

@Injectable()
export class TagsService extends UiDataGridService {
  public icon = 'icon-event'
  public title = 'Tags'
  public files: any[] = []
  public selectedTag: ContentTag

  public tableColumns = [
    { field: 'name', label: 'Name', action: 'edit' },
    { field: 'location', label: 'Location' },
  ]

  constructor(
    private domainApi: SystemDomainApi,
    private formService: FormService
  ) {
    super()
    this.columns = this.tableColumns
  }

  setSelectedTag(tag: ContentTag) {
    this.selectedTag = tag
  }

  getFormFields() {
    return [
      this.formService.input('name', {
        label: 'Name',
        placeholder: 'Name',
      }),
      this.formService.wysiwyg('description', {
        label: 'Description',
        placeholder: 'Description',
      }),
      this.formService.date('date', {
        label: 'Date',
      }),
      this.formService.input('location', {
        label: 'Location',
        placeholder: 'Location',
      }),
      this.formService.select('storageFileId', {
        label: 'File',
        options: this.files,
      }),
    ]
  }

  getFormConfig() {
    return {
      icon: this.icon,
      fields: this.getFormFields(),
      showCancel: true,
      hasHeader: false,
    }
  }

  getFiles() {
    this.domainApi
      .getStorageFiles(this.domain.id)
      .subscribe(files =>
        files.map(file => this.files.push({ value: file.id, label: file.name }))
      )
  }

  getItems(): Observable<ContentTag[]> {
    return this.domainApi.getContentTags(
      this.domain.id,
      this.getFilters({ include: ['storageFile'] })
    )
  }

  getItem(id): Observable<ContentTag> {
    return this.domainApi.findByIdContentTags(this.domain.id, id)
  }

  getItemCount(): Observable<any> {
    return this.domainApi.countContentTags(
      this.domain.id,
      this.getWhereFilters()
    )
  }

  upsertItem(item, successCb, errorCb): Subscription {
    if (item.id) {
      return this.upsertTag(item, successCb, errorCb)
    }
    return this.createTag(item, successCb, errorCb)
  }

  upsertTag(item, successCb, errorCb): Subscription {
    return this.domainApi
      .updateByIdContentTags(this.domain.id, item.id, item)
      .subscribe(successCb, errorCb)
  }

  createTag(item, successCb, errorCb): Subscription {
    return this.domainApi
      .createContentTags(this.domain.id, item)
      .subscribe(successCb, errorCb)
  }

  deleteItem(item, successCb, errorCb): Subscription {
    return this.domainApi
      .destroyByIdContentTags(this.domain.id, item.id)
      .subscribe(successCb, errorCb)
  }
}
