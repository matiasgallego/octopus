import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { UiService } from '@colmena/admin-ui'

import { ContentTag, TagsService } from '../tags.service'

@Component({
  selector: 'app-tag-form',
  template: `
  <div class="row">
    <div class="col-md-6">
      <ui-form *ngIf="item" [config]="formConfig" [item]="item" (action)="handleAction($event)"></ui-form>
    </div>
    <div class="col-md-6">
      <app-content-tag [item]="item"></app-content-tag>
    </div>
  </div>
  `,
})
export class TagFormComponent implements OnInit {
  public formConfig: any = {}
  public item: any

  constructor(
    private service: TagsService,
    private uiService: UiService,
    private router: Router
  ) {}

  ngOnInit() {
    this.item = this.service.selectedTag || new ContentTag()
    this.formConfig = this.service.getFormConfig()
  }

  handleAction(event) {
    switch (event.action) {
      case 'save':
        return this.service.upsertItem(
          event.item,
          () => {
            this.uiService.toastSuccess(
              'Save Tag Success',
              `<u>${event.item.name}</u> has been saved successfully`
            )
            this.handleAction({ action: 'cancel' })
          },
          err => this.uiService.toastError('Save Tag Fail', err.message)
        )
      case 'cancel':
        return this.router.navigate(['/content/tags'])
      default:
        return console.log('Unknown Event Action:', event)
    }
  }
}
