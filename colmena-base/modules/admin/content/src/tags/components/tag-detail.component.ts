import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { UiTabLink } from '@colmena/admin-ui'

import { TagsService } from '../tags.service'

@Component({
  selector: 'app-tag-detail',
  template: `
    <ui-page [tabs]="tabs" [title]="item ? item.name : 'Add New Tag'">
      <router-outlet></router-outlet>
    </ui-page>
  `,
})
export class TagDetailComponent implements OnInit {
  public tabs: UiTabLink[] = [
    { icon: 'fa fa-pencil', title: 'Edit', link: 'edit' },
  ]

  public item: any

  constructor(private service: TagsService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.item = this.route.snapshot.data.tag

    if (!this.item) {
      this.tabs = [{ icon: 'fa fa-plus', title: 'Create', link: '' }]
    }
    this.service.setSelectedTag(this.item)
  }
}
