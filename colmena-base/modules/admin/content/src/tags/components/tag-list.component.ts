import { Component, ViewChild } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { UiService } from '@colmena/admin-ui'

import { TagsService } from '../tags.service'

@Component({
  selector: 'app-tag-list',
  template: `
    <div class="card">
      <div class="card-block">
        <ui-data-grid #grid (action)="action($event)" [service]="service"></ui-data-grid>
      </div>
    </div>
  `,
})
export class TagListComponent {
  @ViewChild('grid') private grid

  constructor(
    public service: TagsService,
    private uiService: UiService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.service.domain = this.route.snapshot.data['domain']
    this.service.getFiles()
  }

  action(tag) {
    switch (tag.action) {
      case 'edit':
        return this.router.navigate([tag.item.id], {
          relativeTo: this.route.parent,
        })
      case 'add':
        return this.router.navigate(['create'], {
          relativeTo: this.route.parent,
        })
      case 'delete':
        const successCb = () =>
          this.service.deleteItem(
            tag.item,
            () => this.grid.refreshData(),
            err => this.uiService.toastError('Error Deleting item', err.message)
          )
        const question = {
          title: 'Are You Sure?',
          text: 'The action can not be undone.',
        }
        return this.uiService.alertQuestion(question, successCb, () => ({}))
      default:
        return console.log('Unknown Tag Action', tag)
    }
  }
}
