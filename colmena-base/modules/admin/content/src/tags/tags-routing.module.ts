import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { TagDetailComponent } from './components/tag-detail.component'
import { TagFormComponent } from './components/tag-form.component'
import { TagListComponent } from './components/tag-list.component'

import { TagsResolver } from './tags.resolvers'

const routes: Routes = [
  {
    path: '',
    data: { title: 'Tags' },
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: TagListComponent,
        data: { title: 'List' },
      },
      {
        path: 'create',
        component: TagDetailComponent,
        data: { title: 'Create' },
        children: [
          {
            path: '',
            component: TagFormComponent,
          },
        ],
      },
      {
        path: ':id',
        component: TagDetailComponent,
        resolve: {
          tag: TagsResolver,
        },
        data: { title: 'Tag' },
        children: [
          { path: '', redirectTo: 'edit', pathMatch: 'full' },
          {
            path: 'edit',
            component: TagFormComponent,
            data: { title: 'Edit' },
          },
        ],
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TagsRoutingModule {}
