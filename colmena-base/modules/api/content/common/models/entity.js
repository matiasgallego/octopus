'use strict'

module.exports = function Entity(ContentEntity) {
    
    // ContentEntity.observe('access', function(ctx, next) {
    //     console.log(ctx.scope);
    //     next();
    // });

    ContentEntity.getFromParseHub = function(project_token, api_key, cb) {
        callParseHubAPI(project_token, api_key,
            function(err, result, response) {
                if(response.body.error) {
                    console.log("Error haciendo GET a PARSEHUB",response.body.error);
                    cb(null,response.body.error);
                } else {
                    console.log(response.body);
                    cb(null,response.body);
                }
            }
        );
    }

    ContentEntity.remoteMethod('getFromParseHub', {
        accepts: [
            {arg: 'project_token', type: 'string'},
            {arg: 'api_key', type: 'string'}
        ],
        returns: {arg: 'result', type: 'Object'},
        http: {path:'/parsehub', verb: 'get'}
    });

    var callParseHubAPI = function(project_token, api_key, callback) {
        var parsehubAPIService = ContentEntity.app.dataSources.ParseHubApi;
        parsehubAPIService.lastReadyRun.call(parsehubAPIService, project_token, api_key, callback);
    };

    ContentEntity.importFromMatterportAPI = function(entity_id, user_id, matterport_model_id, video_url, matterport_access_token, cb) {

        // add a matterport access token by default
        if(!matterport_access_token || !matterport_access_token.length) {
            matterport_access_token = '47e082c90bcf243921de2801b033f10bffd3978e';
        }

        callImportModelFromMatterportAPI(matterport_model_id, matterport_access_token,
            function(err, result, response) {
              if(response.body.error) {
                console.log("Error haciendo GET a Matterport",response.body.error);
                cb(null,response.body.error);
              } else {
                var model = response.body;
                // console.log("Importe Modelo:",matterport_model_id, model );
                // advert.updateAttribute('facebookPostId', response.body.id, function(err, instance) {});
                // entity.updateAttribute('facebookPublicationsInPeriod', entity.facebookPublicationsInPeriod+1, function(err, instance) {});
                
                var Container = ContentEntity.app.models.StorageContainer;
                var containerName = "model_" + matterport_model_id;

                Container.createContainer({
                  name: containerName
                },function(err,c) {
                  if(err) {
                    console.log("ERROR CREANDO CONTAINER:",err.message);
                  }
                });

                let images = []
                let panos = []
                let modelImage = null

                //104.236.104.148

                // Agrego el plano por defecto con el nombre "photo_plano.png"
                //images.push("http://104.236.104.148:3000/api/v1/StorageContainers/"+containerName+"/download/photo_plano.png");

                let image_path = "http://104.236.104.148:3000/api/v1/StorageContainers/"+containerName+"/download/";

                if(model.images) {
                    model.images.forEach(function(snapshoot) {
                        let img_name = snapshoot.name+'.jpg'
                        if(snapshoot.category.localeCompare('panorama')==0) {
                            img_name = 'pano_' + img_name
                            panos.push(image_path + img_name);
                        } else {
                            img_name = 'photo_' + img_name
                            images.push(image_path + img_name);
                        }
                        
                        Container.importUrl(snapshoot.src,containerName,img_name)
                        
                        if(snapshoot.name==(model.image.substring(model.image.lastIndexOf('/')+1,model.image.lastIndexOf('.jpg')))) {
                            modelImage = image_path + img_name;
                        }

                    }, this);
                }

                console.log(modelImage);
                
                let entityIdToUpdate = entity_id != 0 ? entity_id : ''
                setTimeout(function() {
                    ContentEntity.upsert({
                        id: entityIdToUpdate,
                        name: model.name,
                        title: model.name,
                        address: model.address,
                        image: modelImage || model.image,
                        thumbnail: model.thumbnail,
                        url: model.url,
                        matterportModelId: model.sid,
                        media: "https://my.matterport.com/show/?m=" + model.sid + "&lang=es&help=1",
                        systemDomainId: "vrioestudio",
                        systemUserId: user_id,
                        images: images,
                        panorama: panos,
                        videos: [],
                        slideshow: video_url,
                        photopack: "http://104.236.104.148:3000/api/v1/StorageContainers/"+containerName+"/download/photo-pack.zip",
                        panopack: "http://104.236.104.148:3000/api/v1/StorageContainers/"+containerName+"/download/pano-pack.zip"
                    }, function(err, entity) {
                        if(err) {
                          console.error("No se pudo crear ContentEntity para Modelo Matterport",model.id);
                          console.error(err);
                        } else {
                          console.log("Modelo Matterport Importado con exito!",model.sid);
                          
                          // creando los zip de imagenes
                          // 'zip -r pano-pack.zip pano*'
                          // sh /root/storage/zip.sh
                          const exec = require('child_process').exec;
                          var yourscript = exec('pwd',
                            (error, stdout, stderr) => {
                                console.log(`${stdout}`);
                                console.log(`${stderr}`);
                                if (error !== null) {
                                    console.log(`exec error: ${error}`);
                                }
                            }
                          );
      
                          cb(null,entity);
                        }
                    });

                },3000);
                
              }

            });
    }

    ContentEntity.remoteMethod('importFromMatterportAPI', {
        accepts: [
            {arg: 'entity_id', type: 'string'},
            {arg: 'user_id', type: 'string'},
            {arg: 'matterport_model_id', type: 'string'},
            {arg: 'video_url', type: 'string'},
            {arg: 'matterport_access_token', type: 'string'}
            
        ],
        returns: {arg: 'model', type: 'Object'},
        http: {path:'/import', verb: 'get'}
    });

    var callImportModelFromMatterportAPI = function(modelid, access_token, callback) {
        var matterportAPIService = ContentEntity.app.dataSources.MatterportApi;
        matterportAPIService.importModel.call(matterportAPIService, modelid, access_token, callback);
    };
}
