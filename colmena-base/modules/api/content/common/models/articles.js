'use strict'

module.exports = function Entity(ContentArticles) {
    
    ContentArticles.getParsehubRun = function(run_token, api_key, domainId, cb) {

        var request = require('request');
        request({
          uri: 'https://parsehub.com/api/v2/runs/' + run_token + '/data',
          method: 'GET',
          gzip: true,
          qs: {
            api_key: api_key
          }
        }, function(err, resp, body) {
            if(err) {
                cb(null,err);
                return;
            }
            
            var bodyObject = JSON.parse(body);
            
            bodyObject.Posts.forEach(function(element) {
                console.log(element);
                ContentArticles.upsert({
                    entry: element.Entry,
                    entry_url: element.Entry_url,
                    author: element.Author,
                    author_url: element.Author_url,
                    date: element.Date,
                    location: element.Location,
                    location_url: element.Location_url,
                    systemDomainId: domainId,
                });  
            }, this);

            cb(null,bodyObject.Posts.length);
        });

        return;

        // var zlib = require('zlib');
        // callParseHubAPI_getRun(run_token, api_key,
        //     function(err, result, response) {
        //         if(response.body.error) {
        //             console.log("Error en getParsehubRun()",response.body.error);
        //             cb(null,response.body.error);
        //         } else {
        //             var output = '';

        //             switch (response.headers['content-encoding']) {
        //                 case 'gzip':
        //                     zlib.inflate(result, function(err,buf){
        //                         if(err) {
        //                             console.log('ERROR',err);
        //                         } else {
        //                             console.log('OK',buf);
        //                         }
        //                         cb(null,buf);
        //                     });
        //                     break;
        //                 case 'deflate':
        //                     output = zlib.createInflate(result);
        //                     cb(null,output);
        //                     break;
        //                 default:
        //                     cb(null,result);
        //                   break;
        //             }
        //         }
        //     }
        // );
    }

    ContentArticles.remoteMethod('getParsehubRun', {
        accepts: [
            {arg: 'run_token', type: 'string'},
            {arg: 'api_key', type: 'string'},
            {arg: 'domain_id', type: 'string'}
        ],
        returns: {arg: 'result', type: 'Object'},
        http: {path:'/getparsehubrun', verb: 'get'}
    });

    var callParseHubAPI_getRun = function(run_token, api_key, domain_id, callback) {
        var parsehubAPIService = ContentArticles.app.dataSources.ParseHubApi;
        parsehubAPIService.getRun.call(parsehubAPIService, run_token, api_key, domain_id, callback);
    };


    ContentArticles.getParsehubLastReadyRun = function(project_token, api_key, cb) {
        callParseHubAPI_lastReadyRun(project_token, api_key,
            function(err, result, response) {
                console.log(response);
                if(response.body.error) {
                    console.log("Error en getParsehubLastReadyRun()",response.body.error);
                    cb(null,response.body.error);
                } else {
                    console.log(response.body);
                    cb(null,response.body);
                }
            }
        );
    }

    ContentArticles.remoteMethod('getParsehubLastReadyRun', {
        accepts: [
            {arg: 'project_token', type: 'string'},
            {arg: 'api_key', type: 'string'}
        ],
        returns: {arg: 'result', type: 'Object'},
        http: {path:'/parsehublastreadyrun', verb: 'get'}
    });

    var callParseHubAPI_lastReadyRun = function(project_token, api_key, callback) {
        var parsehubAPIService = ContentArticles.app.dataSources.ParseHubApi;
        parsehubAPIService.lastReadyRun.call(parsehubAPIService, project_token, api_key, callback);
    };

}
