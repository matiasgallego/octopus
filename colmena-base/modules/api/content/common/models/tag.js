module.exports = function(ContentTag) {
    
      ContentTag.observe('before save', function(ctx, next) {
    
        if (ctx.data) {
          // viene de admin, las entidades que se agregaron a esta tag
          ctx.hookState.hasEntities = ctx.data.hasEntities
          delete ctx.data.hasEntities;
        }
    
        next();
      });
    
      ContentTag.observe('after save', function(ctx, next) {
    
        if(ctx.hookState.hasEntities) {
    
          ctx.instance.entities(function(err,entities) {
            for (var i = 0; i < entities.length; i++) {
              if(ctx.hookState.hasEntities.indexOf(String(entities[i].id)) == -1) {
                ctx.instance.entities.remove(entities[i], function(err) {
                });
              }
            }
          });
    
          for (var i = 0; i < ctx.hookState.hasEntities.length; i++) {
            var Entity = ContentTag.app.models.Entity;
            Entity.findById(ctx.hookState.hasEntities[i],function(err,entity){
              ctx.instance.entities.add(entity, function(err) {
                console.log('agregando tag',entity.name);
              });
            });
          }
        }
    
        next();
      });
    
      ContentTag.observe('before delete', function(ctx, next) {
    
        ContentTag.findById(ctx.where.id, function(err, tag){
    
          // borro los links entre esta entidad y sus categorias
          tag.entities(function(err,entities) {
            for (var i = 0; i < entities.length; i++) {
              tag.entities.remove(entities[i], function(err) {
                if(err) {
                  console.log(err);
                }
              });
            }
          });
    
          next();
        });
      });
    
    };